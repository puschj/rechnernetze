package de.jojoma;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	// Set the port number.
	private static final int PORT = 1055;

	private static ServerSocket serverSocket;

	public static void main(String[] args) throws IOException {

		// listen to http port
		serverSocket = new ServerSocket(PORT);

		while (true) {
			try {
				// when a client connects, pass the connection
				// socket to a new ClientHandler thread
				Socket s = serverSocket.accept();
				new Thread(new ClientHandler(s)).start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
