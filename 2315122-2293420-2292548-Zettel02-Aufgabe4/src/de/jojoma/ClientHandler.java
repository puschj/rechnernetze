package de.jojoma;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.StringTokenizer;

class ClientHandler implements Runnable {
	
	private Socket socket;

	public ClientHandler(Socket s) {
		socket = s;
	}

	public void run() {
		
		BufferedReader in = null;
		PrintStream out = null;
		String filename = "";
		
		try {
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out = new PrintStream(new BufferedOutputStream(
					socket.getOutputStream()));

			// read and log filename
			String s = in.readLine();
			System.out.println(s);

			StringTokenizer tokenizer = new StringTokenizer(s);

			// check correct header format
			if (tokenizer.hasMoreElements()
					&& tokenizer.nextToken().equalsIgnoreCase("GET")
					&& tokenizer.hasMoreElements()) {
				filename = tokenizer.nextToken();
			} else {
				throw new FileNotFoundException();
			}

			// assume request for index.html, when
			// address ends with '/'
			if (filename.endsWith("/")) {
				filename += "index.html";
			}

			// remove trailing seperators
			while (filename.charAt(0) == '/') {
				filename = filename.substring(1);
			}

			// use correct file separators
			filename = filename.replace('/', File.separator.charAt(0));

			// open file
			InputStream f = new FileInputStream(filename);

			// find correct content type
			String contentType = "text/plain";
			if (filename.endsWith(".html") || filename.endsWith(".htm")) {
				contentType = "text/html";
			} else if (filename.endsWith(".jpg") || filename.endsWith(".jpeg")) {
				contentType = "image/jpeg";
			} else if (filename.endsWith(".gif")) {
				contentType = "image/gif";
			}

			// write header
			out.print("HTTP/1.0 200 OK\r\n" + "Content-type: " + contentType
					+ "\r\n\r\n");

			// write body (file)
			byte[] a = new byte[4096];
			int n;
			while ((n = f.read(a)) > 0) {
				out.write(a, 0, n);
			}
			out.close();

		} catch (FileNotFoundException e) {
			// if ressource is not found
			out.println("HTTP/1.0 404 Not Found\r\n"
					+ "Content-type: text/html\r\n\r\n"
					+ "<html><head></head><body>" + filename
					+ " not found</body></html>\n");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}