package de.umr.cn.traceroute;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class NetworkUtils {
    
    private static final int PROBES = 3;
    private static final int MAX_HOPS = 30;
    private static final int MAX_TIME = 5000;
    
    private static class ICMPAnswer {
        String address;
        boolean expired;
        
        public ICMPAnswer(String host, boolean expired) {
            this.address = host;
            this.expired = expired;
        }
        
        @Override
        public String toString() {
            return "ICMP("+address+", "+expired+")";
        }
    }
    
    private static ICMPAnswer ping(int ttl, String target) throws IOException, InterruptedException {
        Runtime rt = Runtime.getRuntime();
        String[] commands = {"ping", "-w", Integer.toString(MAX_TIME),"-n", "1", "-i", Integer.toString(ttl), target};

        Process proc = rt.exec(commands);
        InputStream stdin = proc.getInputStream();
        InputStreamReader isr = new InputStreamReader(stdin);
        BufferedReader br = new BufferedReader(isr);

        String line = null;
        br.readLine();
        br.readLine();
        if ( (line = br.readLine()) != null) {
            if (line.contains("timed out")) return null;
            String address = line.split(" ")[2];
            address = address.substring(0, address.length() - 1);
            boolean expired = line.contains("expired");
            return new ICMPAnswer(address, expired);
        }
        return null;
    }
    
    public static String nslookup(String address) throws IOException {
        Runtime rt = Runtime.getRuntime();
        String[] commands = {"nslookup", address};
        
        Process proc = rt.exec(commands);
        InputStream stdin = proc.getInputStream();
        InputStreamReader isr = new InputStreamReader(stdin);
        BufferedReader br = new BufferedReader(isr);
        
        String line = null;
        for (int i = 0; i < 3; i++) br.readLine();
        if ( (line = br.readLine()) != null) {
            return line.split("    ")[1];
        }
        return "-";
    }
    
    public static void traceroute(String target) throws IOException, InterruptedException {
        
        int n = 1;
        boolean done = false;
        boolean probeReturned;
        String lastAddress = null;
        
        System.out.printf("%4s  %-40s  %-20s\n", "#Hop", "DNS-Name", "IP-Address");
        
        while((!done) && n <= MAX_HOPS) {
            probeReturned = false;
            for (int i = 0; i < PROBES; i++) {
                ICMPAnswer answer = ping(n, target);
                if (answer == null) {
                    continue;
                } else {
                    probeReturned = true;
                }
                if (answer.expired) {
                    String address = answer.address;
                    if (!address.equals(lastAddress)) {
                        System.out.printf("%-4d  %-40s  %-20s\n", n, nslookup(address), address);
                        lastAddress = address;
                    }
                } else {
                    done = true;
                }
            }
            if (!probeReturned) {
                System.out.println("all probes timed out...");
                break;
            }
            n += 1;
        }
    }
    
    public static void main(String[] args) throws IOException, InterruptedException {
        traceroute("stackoverflow.com");
    }

}
