package de.jojo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * This code is partly stolen from
 * http://forum.codecall.net/topic/66335-how-to-connect
 * -to-pop3-server-using-java-socket/ because the reader.readLine(); command
 * made problems when parsing more lines at once.
 * 
 * 
 */
public class Client {

	private Socket socket;

	private BufferedReader reader;
	private BufferedWriter writer;

	private static final int DEFAULT_PORT = 110;

	public void connect(String host) {
		connect(host, DEFAULT_PORT);
	}

	public void connect(String host, int port) {
		socket = new Socket();
		try {
			socket.connect(new InetSocketAddress(host, port));
			reader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(
					socket.getOutputStream()));

		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Connected to the host");
		readResponseLine();
	}

	protected String sendCommand(String command) {

		System.out.println(command);

		try {
			writer.write(command + "\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readResponseLine();
	}

	protected String readResponseLine() {

		String response = "";
		try {
			response = reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("[in] : " + response);

		return response;
	}

	protected String getMessage(int i) {

		String response = sendCommand("RETR " + i);

		StringBuilder sb = new StringBuilder();

		// process headers
		while ((response = readResponseLine()).length() != 0) {
			if (response.startsWith("\t"))
				continue; // no process of multiline headers

			sb.append(response + "\n");
		}

		// process body
		while (!(response = readResponseLine()).equals(".")) {
			sb.append(response + "\n");
		}

		return sb.toString();
	}

	public void login(String username, String password) {
		sendCommand("USER " + username);
		sendCommand("PASS " + password);
	}

	public List<String> getMessages(int max) {
		List<String> messageList = new ArrayList<String>();
		if (max > 0) {
			for (int i = 1; i < max + 1; i++) {
				try {
					messageList.add(getMessage(i));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return messageList;
	}

	public void logout() {
		sendCommand("QUIT");
	}

	public void disconnect() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		reader = null;
		writer = null;
		System.out.println("Disconnected from the host");
	}

}
