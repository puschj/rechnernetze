package de.jojo;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

public class Main {

	private final static String HOST = "pophost.mathematik.uni-marburg.de";

	private static String USER = "";
	private static String PASS = "";

	private static final int MAX_2_DOWNLOAD = 10;

	public static void main(String[] args) {
		Client client = new Client();

		client.connect(HOST);

		/**
		 * user/pw input
		 */
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter your username: ");
		USER = scanner.next();
		System.out.print("Enter your password: ");
		PASS = scanner.next();

		scanner.close();

		/**
		 * trying to log in
		 */
		client.login(USER, PASS);
		List<String> messages = client.getMessages(MAX_2_DOWNLOAD);

		for (int index = 0; index < messages.size(); index++) {
			PrintWriter out = null;
			try {
				out = new PrintWriter("email_" + String.format("%03d", index)
						+ ".msg");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			out.println(messages.get(index).toString());
			out.close();
		}
		client.logout();
		client.disconnect();
	}
}
