package de.jojoma;

import java.util.LinkedList;
import java.util.List;

public class JacobsonKarel {

    /**
     * 
     * @param sampleRTTs
     *            the list of measured sample RTTs
     * @param estimatedRTT0
     *            the initial estimatedRTT
     * @param deviation0
     *            the initial deviation
     * @param delta
     *            the delta value of the computation
     * @param stopC
     *            a StopCriterion for interrupting the calculation on certain
     *            values
     * @return <tt>false</tt> if the calculation was stopped by the StopCriterion
     *         ,<tt>true</tt> otherwise
     */
    public static boolean printTimeouts(List<Integer> sampleRTTs,
            double estimatedRTT0, double deviation0, double delta,
            StopCriterion stopC) {
        List<Double> timeouts = new LinkedList<Double>();
        double estimatedRTT = estimatedRTT0;
        double deviation = deviation0;

        System.out.println("=== Starting Jacobson/Karel Algorithm ===");
        System.out.printf("Rnd  Smp Est  Dev  Timeout\n");
        System.out.printf("%3d: %3s %-2.2f %-2.2f %5s\n", 0, "", estimatedRTT,
                deviation, "");

        for (int i = 0; i < sampleRTTs.size(); i++) {
            int sampleRTT = sampleRTTs.get(i);
            double difference = sampleRTT - estimatedRTT;
            estimatedRTT += (delta * difference);
            deviation += delta * (Math.abs(difference) - deviation);
            double timeout = estimatedRTT + 4 * deviation;
            timeouts.add(timeout);
            System.out.printf("%3d: %-3d %-2.2f %-2.2f %-2.2f\n", i + 1,
                    sampleRTT, estimatedRTT, deviation, timeout);
            if (stopC.checkStop(sampleRTT, estimatedRTT, deviation, timeout))
                return false;
        }
        return true;
    }

    interface StopCriterion {
        public boolean checkStop(int sampleRTT, double estimatedRTT,
                double deviation, double timeout);
    }

    public static void main(String[] args) {
        List<Integer> sampleRTTs = new LinkedList<Integer>();
        final int SAMPLE_SIZE = 100;

        // task a)
        System.out.println("== TASK a) ==");
        for (int i = 0; i < SAMPLE_SIZE; i++)
            sampleRTTs.add(1);
        StopCriterion isBelowFour = new StopCriterion() {
            @Override
            public boolean checkStop(int sampleRTT, double estimatedRTT,
                    double deviation, double timeout) {
                boolean stop = timeout < 4.0;
                if (stop)
                    System.out.println("STOP: Timeout below 4.0");
                return stop;
            }
        };
        printTimeouts(sampleRTTs, 4.0, 1.0, 1.0 / 8.0, isBelowFour);

        // task b)
        System.out.println("\n== TASK b) ==");
        sampleRTTs.clear();
        StopCriterion isTimeout = new StopCriterion() {
            @Override
            public boolean checkStop(int sampleRTT, double estimatedRTT,
                    double deviation, double timeout) {
                boolean stop = timeout < sampleRTT;
                if (stop)
                    System.out.println(" ==> Timeout");
                return stop;
            }
        };
        boolean b = true;
        for (int N = 1; b ; N++) {
            sampleRTTs.clear();
            System.out.println("\nEvery Nth sampleRTT is 4.0 - N = "+N);
            for (int i = 0; i < SAMPLE_SIZE; i++)
                sampleRTTs.add(i % N == (N - 1) ? 4 : 1);
            b = printTimeouts(sampleRTTs, 1.0, 1.0, 1.0 / 8.0, isTimeout);
            if (!b) System.out.println("A Timeout occurs for N = "+N);
        }
    }

}
