// Einfachster Routing-Simulator
//
// AG Verteilte Systeme, Uni Marburg

import java.util.Vector;
import java.util.Enumeration;

public class Router extends Thread {

    // Die Anzahl der Knoten im Netz
    static final int ANZKNOTEN = 4;

    // Die Referenzenliste der Knoten
    static Router[] router = new Router[ANZKNOTEN];

    // Der Adjazenzvektor des Knotens
    int[]  adjazenzvektor = new int[ANZKNOTEN];

    // ID des Routers
    private int id = 0;

    // Die Queue fuer empfangene Nachrichten
    private Vector queue   = new Vector();

    // Die Tabelle Vector -> TabellenElement*
    // Vector: Thread-safe
    private Vector tabelle = new Vector();

    // Ein Konstruktor...
    public Router(int id){
	System.out.println("Router " + id + ": instanziiert");
	this.id = id;
	// Noch ist kein anderer Knoten zu erreichen,
	for (int i=0; i<ANZKNOTEN; i++)
	    adjazenzvektor[i]=Integer.MAX_VALUE;
	// und die Tabelle kennt sich nur selbst
	tabelle.add(new TabellenElement(id, 0, -1));
    }

    // Allen benachbarten Knoten die eigene Tabelle schicken
    private void benachrichtigen(){
	TabellenElement.showTabelle(tabelle, "Router "+id+": sendet Tabelle an Nachbarn");
	// Alle im Adjazenzvektor erreichbaren Knoten werden benachrichtigt
	for (int i=0; i<ANZKNOTEN; i++)
	    if (adjazenzvektor[i]<Integer.MAX_VALUE)
		router[i].send(tabelle);
    }

    // Einfuegen einer Verbindung zu einem anderen Knoten
    public void insert(int id, int dist){
	System.out.println("Router " + this.id + 
			   ": insert ("+id+", "+dist+")");
	adjazenzvektor[id]=dist;
	tabelle.add(new TabellenElement(id, dist,id));
	benachrichtigen();
    }

    // Des Threads Kern. Ein Thread wird mit start gestartet, 
    // run wird intern aufgerufen!
    public void run(){
	System.out.println("Router " + id + ": gestartet");
	try {
	    while (!isInterrupted()){
		// Warten auf eine Nachricht
		while (queue.isEmpty())
		    sleep(50);
		

		// Holen der Nachricht
		Vector message = (Vector) queue.firstElement();
		queue.remove(0);
		
		TabellenElement.showTabelle(message, "Router " + id + ": Erhaltene Nachricht:");


		boolean veraendert = false;
		Enumeration enum = message.elements();

		int senderid = -1;

		// Sendersuche. In der Nachricht muss die Entfernung zum Sender 0 sein
		while (enum.hasMoreElements()){
		    TabellenElement te = (TabellenElement) enum.nextElement();
		    if (te.distanz==0){
			senderid = te.knoten;
			break;
		    }
		}

		// Der Routing-Algorithmus...
		
		int distToSender = adjazenzvektor[senderid];
		if (distToSender < 0) return;
		for (int i = 0; i < ANZKNOTEN; i++) {
		    TabellenElement receivedTE = TabellenElement.getTabellenElementById(message, i);
		    if (receivedTE == null) continue;
		    int newDist = (receivedTE.distanz + distToSender);
		    TabellenElement currentTE = TabellenElement.getTabellenElementById(tabelle, i);
		    if (currentTE == null) {
		        currentTE = new TabellenElement(i, Integer.MAX_VALUE, -1);
		        tabelle.add(currentTE);
		    }
		    if (newDist < currentTE.distanz) {
		        veraendert = true;
		        currentTE.distanz = newDist;
		        currentTE.nexthop = senderid;
		    }
		}
		
		if (veraendert)
		    benachrichtigen();

	    }
	} catch (InterruptedException e) {
	    System.out.println("Router " + id + ": unterbrochen");
	    return;
	}
    }

    // Sendet einem Router-Thread eine Nachricht in Form einer
    // Tabelle: Vector->TabellenElement*
    public void send (Vector message) {
	queue.add(message.clone());
    }

    public Vector getTabelle() {
        return tabelle;
    }

    // Pausieren
    private static void sleepSec(int s){
	System.out.println("main: Schlafe " + s + " Sekunden");
	try {
	    sleep(s * 1000);
	} catch (InterruptedException e){}
    }

    // Und hier das Programm zum Testen...
    public static void main(String[] args){
	System.out.println("main: Start");

	System.out.println("main: Instanziiere Threads");
	for (int i=0; i<ANZKNOTEN; i++)
	    router[i] = new Router(i);
	

	System.out.println("main: Starte Threads");
	for (int i=0; i<ANZKNOTEN; i++)
	    router[i].start();

	// Ein paar Kanten in den Graphen...
	router[0].insert(1, 1);
	router[1].insert(0, 1);
	
	router[1].insert(2, 1);
	router[2].insert(1, 1);

	router[3].insert(1, 1);
	router[1].insert(3, 1);

	router[3].insert(2, 1);
	router[2].insert(3, 1);

	sleepSec(5);
	for (int i=0; i<ANZKNOTEN; i++)
	    TabellenElement.showTabelle(router[i].getTabelle(), "Finale Tabelle für Router ["+i+"]");

	System.out.println("main: unterbreche Threads");
	for (int i=0; i<ANZKNOTEN; i++){
	    router[i].interrupt();
	    try {
		router[i].join();
	    } catch (InterruptedException e){}
	}
	
	System.out.println("main: Ende");
    }

}
