// TabellenElement
//
// AG Verteilte Systeme, Uni Marburg

import java.util.Vector;
import java.util.Enumeration;

public class TabellenElement {

    // Die ID eines Knoten
    public int knoten = 0;

    // Und die Distanz dahin
    public int distanz = 0;

    // ueber diesen Knoten sollen Pakete dahin gehen
    public int nexthop = 0;

    // Copy-Konstruktor
    public TabellenElement(TabellenElement te){
	knoten = te.knoten;
	distanz = te.distanz;
	nexthop = te.nexthop;
    }

    // Und noch ein Konstruktor
    public TabellenElement(int knoten, int distanz, int nexthop){
	this.knoten = knoten;
	this.distanz = distanz;
	this.nexthop = nexthop;
    }


    // Diese Klassenmethode gibt aus einer Tabelle ein 
    // TabellenElement ueber dessen Knoten zurueck, oder null,
    // wenn es nicht zu finden ist
    public static TabellenElement getTabellenElementById(Vector tabelle, int knoten){
	Enumeration enum = tabelle.elements();
	while (enum.hasMoreElements()){
	    TabellenElement te = (TabellenElement) enum.nextElement();
	    if (te.knoten==knoten)
		return te;
	}
	return null;
    }

    // Ausgabe einer Tabelle
    // snchronized und der mitgegebene String verhindern ein Ueberschneiden 
    // der Ausgabe mehrerer Threads
    synchronized public static void showTabelle (Vector tabelle, String mess){
	System.out.println (mess);
	Enumeration enum = tabelle.elements();
	while (enum.hasMoreElements()){
	    TabellenElement te = (TabellenElement) enum.nextElement();
	    System.out.println("\tKnoten: " + te.knoten +
			       ", Distanz: " + te.distanz +
			       ", Nexthop: " + te.nexthop);
	}
    }

}
