package de.jojoma;

import java.util.StringTokenizer;

import org.apache.commons.codec.binary.Base64;

/**
 * 
 * Der Server antwortet dann mit folgender Nachricht:
 * 
 * #<n> <bytes> <Daten ...>
 * 
 * equals/hashcode only consider the sequenceNumber. In that way no package can
 * be duplicated in a respective collection
 * 
 * @author joachim
 * 
 */
public class ResponsePackage implements Comparable<ResponsePackage> {

	public int sequenceNumber;
	public int fileSize;
	public byte[] data;

	/**
	 * Create a ResponsePackage using a reponse from the Server
	 * 
	 * @param response
	 */
	public ResponsePackage(byte[] response) {

		sequenceNumber = -1;
		fileSize = 0;

		StringTokenizer st = new StringTokenizer(new String(response));
		boolean gotSeqNumber = false;
		boolean gotSizeInfo = false;
		String allData = "";
		String token;
		while (st.hasMoreElements()) {

			token = (String) st.nextElement();
			if (!gotSeqNumber) {
				token = token.replaceAll("#", "");
				sequenceNumber = Integer.parseInt(token);
				gotSeqNumber = true;
			} else if (!gotSizeInfo) {
				fileSize = Integer.parseInt(token);
				gotSizeInfo = true;
			} else {
				allData += new String(Base64.decodeBase64(token));
				// StringUtils .newStringUtf8(Base64.decodeBase64(token));
			}
		}
		data = new byte[allData.getBytes().length];
		data = allData.getBytes();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + sequenceNumber;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponsePackage other = (ResponsePackage) obj;
		if (sequenceNumber != other.sequenceNumber)
			return false;
		return true;
	}

	@Override
	public int compareTo(ResponsePackage o) {
		if (this.sequenceNumber < o.sequenceNumber) {
			return -1;
		}
		if (this.sequenceNumber > o.sequenceNumber) {
			return 1;
		}
		return 0;
	}

}
