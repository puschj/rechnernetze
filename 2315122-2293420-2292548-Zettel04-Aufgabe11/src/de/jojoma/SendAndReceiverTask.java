package de.jojoma;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.concurrent.Callable;

class SendAndReceiverTask implements Callable<byte[]> {

	private DatagramSocket clientSocket;
	private String message;
	private String serverName;
	private int serverPort;

	/**
	 * @param clientSocket
	 * @param message
	 * @param serverName
	 * @param serverPort
	 */
	public SendAndReceiverTask(DatagramSocket clientSocket, String message,
			String serverName, int serverPort) {
		super();
		this.clientSocket = clientSocket;
		this.message = message;
		this.serverName = serverName;
		this.serverPort = serverPort;
	}

	@Override
	public byte[] call() throws Exception {
		send();
		byte[] result = receive();

		return result;
	}

	private boolean send() {
		try {
			byte[] sendData = message.getBytes();

			InetAddress ipAdr = InetAddress.getByName(serverName);

			DatagramPacket sendPacket = new DatagramPacket(sendData,
					sendData.length, ipAdr, serverPort);

			clientSocket.send(sendPacket);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private byte[] receive() {
		try {

			byte[] receiveData = new byte[1034];

			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			clientSocket.receive(receivePacket);

			byte[] realData = Arrays.copyOf(receivePacket.getData(),
					receivePacket.getLength());

			return realData;

		} catch (Exception e) {
			// e.printStackTrace();
		}

		return null;
	}

}