package de.jojoma;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.DatagramSocket;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

class Client {

	public final static int SERVER_PORT = 2342;
	public final static String SERVER_NAME = "pc12921.mathematik.uni-marburg.de";

	/**
	 * The filename of the file that will be created when received all packages
	 */
	public final static String RESULT_FILENAME = "decodedFile.pdf";

	/**
	 * Die MD5-Pruefsumme der Datei, die Sie am Ende empfangen haben, ist
	 * 4dcd2e6c3bf6506ba4e28af1b88c9135.
	 */
	public final static String EXPECTED_CHECKSUM = "4dcd2e6c3bf6506ba4e28af1b88c9135";

	public final static String CLIENT_REQUEST_MESSAGE = "GETFILE";

	/**
	 * Erhaelt der Client nach 1s keine Daten, sendet er das ACK fuer die
	 * zuletzt korrekt empfangene Nachricht erneut.
	 */
	private final static int MAX_SECONDS_TO_WAIT = 1;

	private static DatagramSocket clientSocket;

	/*
	 * TreeSet should return an ordered set, which is what we need.
	 */
	public static TreeSet<ResponsePackage> responsePackages;

	private static ExecutorService executor;

	public static void main(String args[]) throws Exception {

		print("Hello.");

		if (!establishConnection()) {
			print("Couldn't connect to server");

		} else {

			print("Starting downloading all packages...");
			downloadPackages();
			print("Downloaded all packages.");

			print("Creating file out of packages...");
			createFileOutOfPackages(responsePackages, RESULT_FILENAME);
			print("Created file out of packages.");

			if (Utils.isExpectedFile(EXPECTED_CHECKSUM, RESULT_FILENAME)) {
				System.out.println("File has been successfully delivered");
			} else {
				System.out.println("File is broken");
			}

			endConnection();

		}
		print("Good bye.");

	}

	private static void downloadPackages() {

		responsePackages = new TreeSet<ResponsePackage>();

		boolean stillReceivingPackets = true;
		ResponsePackage lastArrivedPackage;

		/**
		 * Der Client sendet den Text GETFILE, um die Transaktion zu starten.
		 */
		byte[] received = sendAndReceive(CLIENT_REQUEST_MESSAGE);
		while (received == null) {
			received = sendAndReceive(CLIENT_REQUEST_MESSAGE);
		}

		lastArrivedPackage = new ResponsePackage(received);
		if (lastArrivedPackage.sequenceNumber > -1
				&& lastArrivedPackage.fileSize > 0) {
			responsePackages.add(lastArrivedPackage);
		}

		/*
		 * After the first one has arrived, process all other packages
		 */
		while (stillReceivingPackets) {

			/**
			 * Hat der Client die Daten korrekt empfangen, bestaetigt er die
			 * Uebertragung mit folgender ACK-Nachricht:
			 * 
			 * #<n+1> <Empfangene bytes>
			 */
			String ackMsg = "#"
					+ Integer.toString(lastArrivedPackage.sequenceNumber + 1)
					+ " " + Integer.toString(lastArrivedPackage.fileSize);
			received = sendAndReceive(ackMsg);

			/**
			 * Der Server antwortet dann mit folgender Nachricht:
			 * 
			 * #<n> <bytes> <Daten ...>
			 */
			while (received == null) {
				received = sendAndReceive(ackMsg);
			}

			ResponsePackage receivedPackage = new ResponsePackage(received);
			if (receivedPackage.sequenceNumber > -1) {

				/**
				 * Erhaelt der Client Daten, die ausserhalb der Sequenznummer
				 * liegen, verwirft er diese und sendet das letzte ACK erneut.
				 */
				if ((receivedPackage.sequenceNumber - lastArrivedPackage.sequenceNumber) != 1) {
					continue;
				}

				if (receivedPackage.fileSize == 0) {
					/**
					 * Die Uebertragung ist beendet, wenn der Server mit einer
					 * korrekten Sequenznummer und 0 Bytes Daten antwortet.
					 */
					stillReceivingPackets = false;
					continue; // or break..
				}

				/*
				 * Only add new package if not already contained
				 */
				if (!responsePackages.contains(receivedPackage)) {
					responsePackages.add(receivedPackage);
				}

				lastArrivedPackage = receivedPackage;

			} else {
				// should not happen
				System.err.println("reveiced message is broken..");
				break;
			}

		} // end of while (stillReceivingPackets)

	}

	private static byte[] sendAndReceive(String msg) {
		Future<byte[]> f = null;
		byte[] response = null;
		try {

			SendAndReceiverTask t = new SendAndReceiverTask(clientSocket, msg,
					SERVER_NAME, SERVER_PORT);
			executor = Executors.newFixedThreadPool(1);

			f = executor.submit(t);

			/*
			 * get a response from the server but only if it doesn't take too
			 * long
			 */
			response = f.get(MAX_SECONDS_TO_WAIT, TimeUnit.SECONDS);

		} catch (Exception e) {
			// e.printStackTrace();
			// System.err.println(String.format("sending took too long.", msg));
			f.cancel(true);
		} finally {

			executor.shutdownNow();
		}
		return response;
	}

	/**
	 * Der Client baut die Verbindung auf
	 */
	private static boolean establishConnection() {
		try {
			clientSocket = new DatagramSocket();

			/*
			 * clientSocket.setSoTimeout is important! Otherwise the
			 * clientSocket will be blocked even if a new request has been sent
			 * already. This is because we don't close the connection of the
			 * clientSocket to the Server, so it wouldn't matter if we start new
			 * sent requests because the clientSocket is still in it's receive
			 * method.
			 */
			clientSocket.setSoTimeout(MAX_SECONDS_TO_WAIT * 1000);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private static boolean endConnection() {
		try {
			clientSocket.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private static void createFileOutOfPackages(
			TreeSet<ResponsePackage> responsePackages, String fileName) {
		FileWriter fw = null;
		try {
			new File(fileName).delete(); // delete if already existing

			FileOutputStream fos = new FileOutputStream(fileName, true);

			for (ResponsePackage rp : responsePackages) {
				fos.write(rp.data);
			}

			fos.flush();
			fos.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (fw != null) {
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static void print(Object text) {
		System.out.println(text + "\n");
	}

}